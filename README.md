# Sunshine #

This weather app build during the [Developing Android Apps](https://www.udacity.com/course/new-android-fundamentals--ud851) course on Udacity.
The original [repository](https://github.com/udacity/ud851-Sunshine).
### Classes not implemented within the course ###
* SunshinePreferences
* FakeDataUtils
* NetworkUtils(not all)
* OpenWeatherJsonUtils
* SunshineDateUtils
* SunshineWeatherUtils
* all test classes